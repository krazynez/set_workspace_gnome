#!/usr/bin/perl
use strict;
use warnings;


# Clear Screen
print "\033[2J";

# Puts cursor at position 0,0
print "\033[0;0H";

print "How many workspaces do you want?: ";

# Gets user input from standard input
my $input = <STDIN>;

# Removes unwanted spacing
chomp $input;

# Creating an array
my @ws;


# Looping through array added workspace names
for(my $i = 0; $i < $input; $i++) {
    my $j = $i + 1;
    print "What is the name for workspace $j: ";
    my $ws = <STDIN>;
    chomp $ws;

# Adding the standard input 'user input' to the end of the array
    push @ws, $ws; 
}
my @tws;
my $ftws;

# Gets the 'Size/Length' of array. e.g. 3 or 4 or 5 etc...
my $ws_length = scalar @ws;


# Creating the fake array for gsettings starts with a "[ like an acual array then ends with a ]"
for(my $j = 0; $j < $ws_length; $j++) {
    if($j eq 0) {
        $ftws = "\"\['$ws[$j]'";
        push @tws, $ftws;

    }    
    elsif($j < $ws_length - 1) {
         $ftws = "'$ws[$j]'";
        push @tws, $ftws;
    }
    else {
    $ftws = "'$ws[$j]'\]\"";
    push @tws, $ftws;

    }

}

# Adding the , and space between each element of the array and turning it into a string
my $fatws = join(', ', @tws);

# preparing gsettings 
my $wsn = "/usr/bin/gsettings set org.gnome.desktop.wm.preferences workspace-names $fatws";
my $wsnu = "/usr/bin/gsettings set org.gnome.desktop.wm.preferences num-workspaces \"$input\"";

# Executing the system function to run the gsettings lines above with the correct data
system($wsn);
system($wsnu);


exit 0;
